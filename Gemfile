# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }
ruby '3.0.3'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'jbuilder', '~> 2.7'
gem 'puma', '~> 4.1'
gem 'rails', '~> 6.0.5', '>= 6.0.5.1'
gem 'sass-rails', '>= 6'
gem 'slim-rails'
gem 'turbolinks', '~> 5'
gem 'webpacker', '~> 4.0'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'stimulus-rails'
gem 'devise'
gem 'omniauth'
gem 'omniauth-google-oauth2'
gem 'omniauth-facebook'
gem 'omniauth-rails_csrf_protection', '~> 1.0'
gem 'figaro'
gem 'enumerize'
gem 'rolify'
gem 'pundit', '~> 2.2'
gem 'ransack'
gem 'image_processing', '>= 1.2'
gem 'redis', '~> 4.0'
gem 'pay', '~> 5.0'
gem 'stripe', '~> 7.0'
gem 'faker'
gem 'noticed', '~> 1.6'
gem 'js-routes'
gem 'aws-sdk-s3', require: false
gem 'pagy'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'sqlite3', '~> 1.4'
  gem 'rspec-rails', '~> 5.0.0'
  gem 'factory_bot_rails'
end

group :development do
  gem 'listen', '~> 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
  gem 'bullet'
  gem 'rubocop-rails', require: false
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'webdrivers'
  gem 'simplecov'
  gem 'shoulda-matchers', '~> 5.0'
end

group :production do
  gem 'pg'
end
