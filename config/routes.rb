Rails.application.routes.draw do
  root 'home#index'
  get :search, to: 'home#search'
  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }  

  resources :users, only: [] do 
    scope module: :users do 
      resources :profile, only: [] do 
        collection do 
          get :overview 
          get :posts 
          get :comments 
          get :saved 
          get :upvoted 
          get :downvoted
        end
      end

      collection do 
        resources :interests, only: %i[create] do 
          get :posts 
          get :communities
        end

        resources :settings, only: [] do 
          collection do 
            get :account
            get :profile
            get :subscription
            post :update_info
          end
        end

        resources :chatrooms, only: %i[index show create] do 
          scope module: :chatrooms do 
            resources :messages, only: %i[create]
            resources :users, only: %i[create]
          end
      
          collection do 
            resources :users, only: [] do 
              get :direct_chatroom, to: 'chatrooms#show_direct'
            end
          end
        end

        resources :subscriptions, only: %i[new create] do 
          collection do 
            get :success
          end
        end

        resources :communities, only: %i[create show edit update] do 
          scope module: :communities do 
            resources :users, only: %i[create] 
            resources :interests, only: %i[create]
      
            get 'posts/:type/new', to: 'posts#new', as: 'posts_new'
            post 'posts/:type', to: 'posts#create', as: 'posts'
      
            resources :community_users, only: [] do 
              post :toggle_status
            end
      
            collection do 
              resources :tags, only: %i[destroy]
            end
          end
      
          get :blocked, to: 'communities#blocked'
        end 

        resources :posts, only: %i[show new create destroy] do 
          scope module: :posts do  
            resources :comments, only: %i[create]
            resources :poll_votes, only: %i[create]
      
            collection do 
              resources :votes, only: %i[create]
            end  
          end

          member do 
            post :toggle_status
            get :toggle_pinned
            get :save, to: 'posts#bookmark'
          end
        end
      end
    end
  end
 
  namespace :admin do 
    resources :communities, only: %i[index] 
  end

  get 'moderator/:id/:status/:type', to: 'moderators#moderate_resources', as: 'moderator_resources'

  namespace :api do 
    namespace :v1 do 
      resources :webhooks, only: %i[create]
    end
  end
end
