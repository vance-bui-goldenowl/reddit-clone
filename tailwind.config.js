const colors = require("tailwindcss/colors");

module.exports = {
  purge: [],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        redditGray: "#dae0e6",
        redditBlue: "#0079d3",
        redditOrange: "#ff4500",
        teal: colors.teal,
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
