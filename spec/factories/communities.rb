# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :community do
    name { Faker::Lorem.word }
    community_type { 'public' }
  end
end
