# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :chatroom do
    name { Faker::FunnyName.two_word_name }
    room_type { 'group' }
  end
end
