# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :interest do
    name { Faker::Hobby.activity }
  end
end
