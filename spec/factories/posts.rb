# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :post do
    title { Faker::Lorem.sentence(word_count: 5) }
    status { 'moderating' }
    pinned { false }
    post_type { 'post' }

    user
    community
    content { '' }
  end
end
