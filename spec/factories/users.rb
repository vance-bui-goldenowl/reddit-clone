# frozen_string_literal: true

require 'faker'

FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password { 'loremipsum123456' }
    password_confirmation { 'loremipsum123456' }
    confirmed_at { 3.days.ago }
    skip_add_customer_id { true }
  end
end
