# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomeController, type: :request do
  include Warden::Test::Helpers
  let(:test_user) { create(:user) }
  let(:test_interest) { create(:interest) }

  describe 'GET /' do
    context 'when user not signed in' do
      it 'populate posts by public communities' do
        get root_path
        expect(response).to have_http_status(:ok)
      end
    end

    context 'when user is signed in' do
      it 'populate posts of both public and private communities that user joined' do
        login_as test_user, scope: :user
        get root_path
        expect(response).to have_http_status(:ok)
      end
    end
  end

  # describe 'POST /add_interests' do
  #   it 'save user\'s interests' do
  #     login_as test_user, scope: :user
  #     user_interests_num = test_user.interests.count
  #     post add_interests_to_user_path, params: { interests_id: [test_interest.id] }
  #     expect(test_user.interests.count).to eq(user_interests_num + 1)
  #   end
  # end
end
