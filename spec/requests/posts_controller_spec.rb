# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Users::PostsController, type: :request do
  include Warden::Test::Helpers

  let(:test_user) { create(:user) }
  let(:test_post) { create(:post) }
  let(:test_community) { create(:community) }

  describe 'GET /posts/:id' do
    # it 'work' do
    #   get post_path(test_post)
    #   expect(response).to have_http_status(:ok)
    # end

    it 'return error when post does not exist' do
      expect { get post_path(100) }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe 'GET /posts/new' do
    it 'work' do
      user = create(:user)
      login_as user, scope: :user

      get new_post_path
      expect(response).to have_http_status(:ok)
    end

    it 'redirect user to login page when user is not authorized' do
      get new_post_path
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe 'GET /communities/:community_id/posts/:type/new' do
    it 'work' do
      login_as test_user, scope: :user

      get community_posts_new_path(community_id: test_community.id, type: 'poll')
      expect(response).to have_http_status(:ok)
    end

    it 'redirect user to login page when user is not authorized' do
      get new_post_path
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to new_user_session_path
    end
  end

  describe 'POST /communities/:community_id/posts/:type' do
    context 'when input is valid' do
      it 'work' do
        login_as test_user, scope: :user
        current_posts_num = Post.all.count

        post community_posts_path(community_id: test_community.id, type: 'poll'),
             params: { post: { title: 'Nothing here' } }

        expect(Post.all.count).to eq(current_posts_num + 1)
        expect(response).to have_http_status(:redirect)
      end
    end

    it 'redirect user to login page when user is not authorized' do
      post community_posts_path(community_id: test_community.id, type: 'poll'),
           params: { post: { title: 'Nothing here' } }
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to new_user_session_path
    end

    context 'when input is invalid' do
      it 'Unable to save' do
        login_as test_user, scope: :user
        current_posts_num = Post.all.count

        post community_posts_path(community_id: test_community.id, type: 'invalid type'),
             params: { post: { title: 'Nothing here' } }

        expect(Post.all.count).to eq(current_posts_num)
        expect(response).to have_http_status(:redirect)
      end
    end
  end

  describe 'POST /post/:id/toggle_status' do
    it 'work' do
      login_as test_user, scope: :user

      post toggle_status_post_path(test_post), params: { post: { status: 'approved' } }
      expect(response).to have_http_status(:redirect)
      expect(Post.last.status).to eq('approved')

      post toggle_status_post_path(test_post), params: { post: { status: 'approved' } }
      expect(response).to have_http_status(:redirect)
      expect(Post.last.status).to eq('moderating')
    end
  end

  describe 'GET /post/:id/toggle_pinned' do
    it "toggle post's pinned status" do
      login_as test_user, scope: :user

      get toggle_pinned_post_path(test_post), xhr: true
      expect(Post.last.pinned).to be_truthy

      get toggle_pinned_post_path(test_post), xhr: true
      expect(Post.last.pinned).to be_falsy
    end
  end

  describe 'GET /posts/:post_id/save' do
    it "toggle post's saved status" do
      login_as test_user, scope: :user
      user_posts_num = test_user.saved_posts.count
      get save_post_path(test_post), xhr: true
      expect(test_user.saved_posts.count).to eq(user_posts_num + 1)
      get save_post_path(test_post), xhr: true
      expect(test_user.saved_posts.count).to eq(user_posts_num)
    end
  end

  describe 'DELETE /posts/:id' do
    context 'when post id to delete is valid' do
      it 'delete post base on id' do
        login_as test_user, scope: :user

        test_post_to_delete = create(:post)
        current_posts_num = Post.all.count

        delete "/users/posts/#{test_post_to_delete.id}"
        expect(response).to have_http_status(:redirect)
        expect(Post.all.count).to eq(current_posts_num - 1)
      end
    end

    context 'when post id to delete is invalid' do
      it "raise error that's post_id doesn't exist" do
        login_as test_user, scope: :user
        current_posts_num = Post.all.count
        expect { delete '/users/posts/50000' }.to raise_error(ActiveRecord::RecordNotFound)
        expect(Post.all.count).to eq(current_posts_num)
      end
    end
  end
end
