# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Users::CommunitiesController, type: :request do
  include Warden::Test::Helpers

  let(:test_private_community) { create(:community, community_type: 'private') }
  let(:test_public_community) { create(:community) }
  let(:test_user) { create(:user) }

  describe 'GET /communities/:id' do
    context 'user is not logged in and community is private' do
      it 'redirect user' do
        get community_path(test_private_community)
        expect(response).to have_http_status(:redirect)
      end
    end

    context 'user is not logged in and community is public' do
      it 'return 200 status' do
        get community_path(test_public_community)
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe 'POST /communities' do
    context 'invalid params' do
      it 'not able save' do
        login_as test_user, scope: :user
        current_communities_num = Community.all.count
        post communities_path, params: { community: { invalid_param: 'lorem' } }
        expect(Community.all.count).to eq(current_communities_num)
      end
    end

    context 'valid params' do
      it 'able to save' do
        login_as test_user, scope: :user
        current_communities_num = Community.all.count
        post communities_path, params: { community: { name: 'lorem', community_type: 'public' } }
        expect(Community.all.count).to eq(current_communities_num + 1)
      end
    end
  end

  describe 'GET /communities/:community_id/add_user' do
    it 'add user to community' do
      login_as test_user, scope: :user
      current_users_num = test_public_community.users.count
      post community_users_path(test_public_community)
      expect(test_public_community.users.count).to eq(current_users_num + 1)
      post community_users_path(test_public_community)
      expect(test_public_community.users.count).to eq(current_users_num + 1)
    end
  end

  describe 'POST /add_interests' do
    it 'add interests to community' do
      login_as test_user, scope: :user
      current_interests_num = test_public_community.interests.count
      test_interest = create(:interest)
      post community_interests_path(test_public_community), params: { interests_id: [test_interest.id] }
      expect(test_public_community.interests.count).to eq(current_interests_num + 1)
    end
  end
end
