# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Vote, type: :model do
  let(:test_user) { build(:user) }
  let(:test_community) { build(:community) }
  let(:test_post) { build(:post) }

  describe 'vote validations' do
    it { is_expected.to validate_presence_of(:value) }
    it { is_expected.to enumerize(:value) }
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:voteable) }
  end
end
