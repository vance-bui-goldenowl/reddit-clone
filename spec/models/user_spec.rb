# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:test_community) { create(:community) }
  let(:test_user) { create(:user) }

  describe '#admin_or_approved_moderator?' do
    it 'determine whether user is admin or moderator' do
      test_user.add_role :member, test_community
      expect(test_user.admin_or_approved_moderator?(test_community)).to be_falsy
      test_user.remove_role :member, test_community
      test_user.add_role :admin
      expect(test_user.admin_or_approved_moderator?(test_community)).to be_truthy
    end
  end

  describe '#active_subscription?' do
    it 'determine if user\'s subscription is still active' do
      expect(test_user.active_subscription?).to be_falsy
      test_user.subscription_status = 'active'
      test_user.subscription_end_date = Time.zone.now + 3.days
      expect(test_user.active_subscription?).to be_truthy
    end
  end

  describe '#moderating_communities' do
    it 'return communities being moderated by user' do
      expect(test_user.moderating_communities.count).to eq(0)
      test_community.add_user(test_user, 'moderator')
      expect(test_user.moderating_communities.count).to eq(1)
    end
  end

  describe '#add_interests' do
    it 'save user\'s interest' do
      test_interest = create(:interest)

      expect(test_user.interests.count).to equal(0)
      test_user.add_interests([test_interest.id])
      expect(test_user.interests.count).to equal(1)
    end
  end
end
