# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:test_user) { build(:user) }
  let(:test_community) { build(:community) }

  describe 'post validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:status) }
    it { is_expected.to enumerize(:status) }
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:community) }
    it { is_expected.to enumerize(:post_type) }
  end

  it 'has an optional content build by rich-text' do
    post = Post.new(
      title: 'Lorem ipsum!',
      user: test_user,
      community: test_community,
      status: 'moderating',
      pinned: false,
      post_type: 'post'
    )
    expect(post.rich_text_content.blank?).to be_truthy
  end

  it 'allowed to be pinned (display first on community page)' do
    post = Post.new(
      title: 'Lorem ipsum!',
      user: test_user,
      community: test_community,
      status: 'moderating',
      post_type: 'post'
    )

    expect(post).to have_attributes(pinned: false)
    post.pinned = true
    expect(post).to have_attributes(pinned: true)
  end
end
