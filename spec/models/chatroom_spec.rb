# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Chatroom, type: :model do
  let(:test_chatroom) { create(:chatroom) }
  let(:test_user) { create(:user) }

  describe 'name validations' do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe 'room_type validations' do
    it { is_expected.to validate_presence_of(:room_type) }
    it { is_expected.to enumerize(:room_type) }
  end

  describe '#users_outside_chatroom' do
    it 'returns users outside chatroom (for adding purpose)' do
      expect(test_chatroom.users_outside_chatroom.count).to eq(User.all.count)
      test_chatroom.users << test_user
      expect(test_chatroom.users_outside_chatroom.count).to eq(User.all.count - 1)
    end
  end

  describe '.find_or_create_direct' do
    it 'returns a new chatroom between 2 new users' do
      prev_chatrooms_num = Chatroom.all.count
      user_one = create(:user)
      user_two = create(:user)
      Chatroom.find_or_create_direct_chatroom(user_one, user_two)
      expect(Chatroom.count).to eq(prev_chatrooms_num + 1)
      prev_chatrooms_num = Chatroom.count
      Chatroom.find_or_create_direct_chatroom(user_one, user_two)
      expect(Chatroom.count).to eq(prev_chatrooms_num)
    end
  end

  describe '#add_users' do
    it 'adds many user at once' do
      expect(test_chatroom.users.count).to eq(0)
      user_one = create(:user)
      user_two = create(:user)
      test_chatroom.add_users([user_one.id, user_two.id])
      expect(test_chatroom.users.count).to eq(2)
    end
  end

  describe '#first_user' do
    it 'returns first user in chatroom after exclude except_id' do
      user_one = create(:user)
      user_two = create(:user)
      test_chatroom.add_users([user_one.id, user_two.id])
      expect(test_chatroom.first_user(user_one.id).id).to eq(user_two.id)
    end
  end
end
