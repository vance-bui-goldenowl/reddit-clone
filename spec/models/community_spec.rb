# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Community, type: :model do
  let!(:test_community) { create(:community) }
  let(:test_user) { create(:user) }
  let(:test_post) { build(:post) }

  describe 'name validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(21) }
    it { is_expected.to validate_uniqueness_of(:name) }
  end

  describe 'community_type validations' do
    it { is_expected.to validate_presence_of(:community_type) }
    it { is_expected.to enumerize(:community_type) }
  end

  describe '#can_be_show?' do
    it 'help to determine where the community can be show' do
      expect(test_community.can_be_show?(test_user)).to be_truthy
    end
  end

  describe '#user_joined?' do
    it 'determine if user joined the community' do
      expect(test_community.user_joined?(test_user)).to be_falsy
    end
  end

  describe '#add_user' do
    it 'add user to community' do
      test_community.add_user(test_user, 'member')
      expect(test_community.users.count).to equal(1)
    end
  end

  describe '#post_by_status' do
    it 'return posts base on input status' do
      expect(test_community.posts_by_status('moderating').count).to eq(0)
      test_community.posts << test_post
      expect(test_community.posts_by_status('moderating').count).to eq(1)
    end
  end

  describe '#moderators' do
    it 'return moderators of community' do
      expect(test_community.moderators.count).to equal(0)
      test_community.add_user(test_user, 'moderator')
      expect(test_community.moderators.count).to equal(1)
    end
  end

  describe '#add_interests' do
    it 'add interest to community' do
      test_interest = create(:interest)

      expect(test_community.interests.count).to equal(0)
      test_community.add_interests([test_interest.id])
      expect(test_community.interests.count).to equal(1)
    end
  end
end
