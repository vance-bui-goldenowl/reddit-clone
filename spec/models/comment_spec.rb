# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:test_user) { build(:user) }
  let(:test_community) { build(:community) }
  let(:test_post) { build(:post) }
  let(:test_comment) { build(:comment) }

  describe 'comment validations' do
    it { is_expected.to validate_presence_of(:content) }
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:post) }

    it 'belongs to parent\'s comment (optional)' do
      comment = Comment.new(
        content: 'lorem',
        user: test_user,
        post: test_post,
        parent_id: nil
      )

      expect(comment).to be_valid
      comment.parent = test_comment
      expect(comment).to be_valid
    end
  end
end
