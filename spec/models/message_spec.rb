# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message, type: :model do
  let(:test_user) { build(:user) }
  let(:test_chatroom) { build(:chatroom) }

  describe 'message validations' do
    it { is_expected.to validate_presence_of(:content) }
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:chatroom) }
  end
end
