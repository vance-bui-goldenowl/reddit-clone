# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TimeFormatHelper, type: :helper do
  describe '#days_from_now' do
    it 'return number of date from now to future' do
      expect(days_from_now(Time.zone.now + 3.days)).to eq('3 days')
    end
  end
end
