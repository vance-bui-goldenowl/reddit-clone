# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CommunitiesHelper, type: :helper do
  let(:test_user) { create(:user) }
  let(:test_community) { create(:community) }

  describe '#user_status_in_community' do
    context 'user is outside community' do
      it 'return Join' do
        expect(user_status_in_community(test_user, test_community)).to eq('Join')
      end
    end

    context 'user is in community' do
      it "return processed text base on user's status" do
        test_community.add_user(test_user, 'member')
        expect(user_status_in_community(test_user, test_community)).to eq('Processing')
        CommunityUser.find_by(community_id: test_community, user_id: test_user).update(status: 'approved')
        expect(user_status_in_community(test_user, test_community)).to eq('Joined')
        CommunityUser.find_by(community_id: test_community, user_id: test_user).update(status: 'rejected')
        expect(user_status_in_community(test_user, test_community)).to eq('Banned')
      end
    end
  end
end
