# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CommentsHelper, type: :helper do
  describe '#format_comment_count' do
    it 'Return numbers of comments base on the amount input' do
      expect(format_comments_count(0)).to eq('0 Comment')
      expect(format_comments_count(2)).to eq('2 Comments')
    end
  end
end
