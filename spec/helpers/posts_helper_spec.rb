# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PostsHelper, type: :helper do
  let(:test_post) { build(:post) }

  describe '#posts_has_image?' do
    it 'check if post has image' do
      expect(post_has_images?(test_post)).to be_falsy
    end
  end

  describe '#post_saved?' do
    context 'when user or post is invalid' do
      it 'return false' do
        expect(post_saved?).to be_falsy
      end
    end

    context 'when both user and post is valid' do
      it 'return base on user has saved post or not' do
        test_user = create(:user)
        test_post = create(:post)
        expect(post_saved?(test_user, test_post)).to be_falsy
      end
    end
  end

  describe '#post_title' do
    it 'return post title, truncate if necessary' do
      test_post.title = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus scelerisque vehicula placerat. Maecenas tristique leo sed ligula iaculis ultrices. Nunc lacinia ornare metus, vitae vehicula justo porttitor a. Etiam congue augue id nunc finibus, sit amet pharetra ex euismod. Ut.'
      expect(post_title(test_post).length).to eq(60)
    end
  end
end
