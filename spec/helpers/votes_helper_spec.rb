# frozen_string_literal: true

require 'rails_helper'

RSpec.describe VotesHelper, type: :helper do
  let(:test_vote) { create(:vote) }
  let(:test_user) { create(:user) }

  describe '#kind_of_vote' do
    context 'user or user\'s vote doesn\'t exist' do
      it 'return None' do
        expect(kind_of_vote(nil, [test_vote])).to eq('None')
        expect(kind_of_vote(test_user, Vote.where(id: test_vote.id))).to eq('None')
      end
    end

    context 'user\'s vote exists' do
      it 'return kind of vote base on vote value' do
        test_vote.update(user_id: test_user.id, value: 1)
        expect(kind_of_vote(test_user, Vote.where(id: test_vote.id))).to eq('Upvote')
        test_vote.update(user_id: test_user.id, value: -1)
        expect(kind_of_vote(test_user, Vote.where(id: test_vote.id))).to eq('Downvote')
      end
    end
  end
end
