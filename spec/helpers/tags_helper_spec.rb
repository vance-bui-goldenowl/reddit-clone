# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TagsHelper, type: :helper do
  describe '#find_tag_with_interest_and_community' do
    it 'return tag id associate between interest and community' do
      test_interest = create(:interest)
      test_community = create(:community)
      expect(find_tag_with_interest_and_community(test_interest, test_community).nil?).to be_truthy
      test_community.interests << test_interest
      expect(find_tag_with_interest_and_community(test_interest, test_community).nil?).to be_falsy
    end
  end
end
