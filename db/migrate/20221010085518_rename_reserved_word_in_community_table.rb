class RenameReservedWordInCommunityTable < ActiveRecord::Migration[6.0]
  def change
    change_table :communities do |t|
      t.rename :type, :community_type
    end
  end
end
