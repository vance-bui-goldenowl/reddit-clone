class CreateTags < ActiveRecord::Migration[6.0]
  def change
    create_table :tags do |t|
      t.references :taggable, polymorphic: true
      t.references :interest, foreign_key: true
      t.timestamps
    end
  end
end
