class CreateUserSavePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :user_save_posts do |t|
      t.belongs_to :user 
      t.belongs_to :post
      t.timestamps
    end
  end
end
