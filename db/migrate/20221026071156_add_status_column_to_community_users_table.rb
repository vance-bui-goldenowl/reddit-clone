class AddStatusColumnToCommunityUsersTable < ActiveRecord::Migration[6.0]
  def change
    add_column :community_users, :status, :string
  end
end
