class ChangeVoteBelongsToFromPostToPolymorphic < ActiveRecord::Migration[6.0]
  def change
    remove_reference :votes, :post
    add_reference :votes, :voteable, polymorphic: true
  end
end
