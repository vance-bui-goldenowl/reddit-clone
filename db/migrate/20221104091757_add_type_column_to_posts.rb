class AddTypeColumnToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :post_type, :string, default: "post"
  end
end
