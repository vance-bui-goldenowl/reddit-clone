class AddNewestMessageSentAtColumn < ActiveRecord::Migration[6.0]
  def change
    add_column :chatrooms, :newest_message_sent_at, :datetime 
  end
end
