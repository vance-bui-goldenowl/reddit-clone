class AddTypeToChatroom < ActiveRecord::Migration[6.0]
  def change
    add_column :chatrooms, :room_type, :string
  end
end
