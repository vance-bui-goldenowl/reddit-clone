class CreatePollOptions < ActiveRecord::Migration[6.0]
  def change
    create_table :poll_options do |t|
      t.string :name 
      t.references :post 
      t.timestamps
    end
  end
end
