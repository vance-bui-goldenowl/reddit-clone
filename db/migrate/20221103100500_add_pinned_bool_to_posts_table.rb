class AddPinnedBoolToPostsTable < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :pinned, :boolean, default: false
  end
end
