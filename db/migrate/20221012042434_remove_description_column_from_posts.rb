class RemoveDescriptionColumnFromPosts < ActiveRecord::Migration[6.0]
  def change
    remove_column :posts, :description
  end
end
