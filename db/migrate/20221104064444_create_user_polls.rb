class CreateUserPolls < ActiveRecord::Migration[6.0]
  def change
    create_table :user_polls do |t|
      t.references :user 
      t.references :poll_option
      t.timestamps
    end
  end
end
