class CreateUserChatrooms < ActiveRecord::Migration[6.0]
  def change
    create_table :user_chatrooms do |t|
      t.belongs_to :user 
      t.belongs_to :chatroom
      t.timestamps
    end
  end
end
