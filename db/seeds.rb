puts "################################"
puts "Seed Interests"

interest_names = ["Gaming", "Sports", "Television", "Celebrity", "Reading", "Crypto", "Business, Economics, and Finance"]

interest_names.each do |name| 
  Interest.find_or_create_by(name: name)
end

puts "################################"
puts "Seed Admin account"

admin_email = "reddit_admin@gmail.com"
unless User.find_by(email: admin_email) 
  account = User.new(
    email: admin_email,
    password: "@redditAdmin1",
    password_confirmation: "@redditAdmin1",
    full_name: "admin1",
    skip_add_customer_id: true
  )
  account.skip_confirmation! 
  account.save 
  account.add_role :admin
end


puts "################################"
puts "Seed Moderators accounts"

moderator_emails = []

(1..5).each do |i|
  moderator_emails << "moderator#{i}@gmail.com"
end

moderator_emails.each_with_index do |email, idx|
  unless User.find_by(email: email) 
    account = User.new(
      email: email, 
      password: "@redditModerator#{idx + 1}",
      password_confirmation: "@redditModerator#{idx + 1}",
      full_name: "moderator#{idx + 1}", 
      skip_add_customer_id: true
    )
    account.skip_confirmation! 
    account.save
  end
end

puts "################################"
puts "Seed Private Communities"

private_community_names = ["Anonymous", "CenturyClub", "SecretsLieHere"]

private_community_names.each_with_index do |name, idx|
  c = Community.find_or_create_by(name: name, community_type: "private")
  c.add_user(User.find_by(email: "moderator#{2 * idx + 1}@gmail.com"), :moderator)
end

puts "################################"
puts "Seed Public Communities"

public_community_names = [
  "Funny",
  "FamousPeople",
  "ReadingAndRelax",
  "ToTheStars",
  "SiscomsToWatch",
  "Gaming",
  "Freakonomics",
  "SportNews",
  "AngryUpvotes",
  "CryptoDudes"
]

public_community_names.each_with_index do |name, idx|
  c = Community.find_or_create_by(name: name, community_type: "public")
  c.add_user(User.find_by(email: "moderator#{(idx + 1) > 5 ? (idx - 4) : (idx + 1)}@gmail.com"), :moderator)
end

puts "################################"
puts "Seeding Complete!"
