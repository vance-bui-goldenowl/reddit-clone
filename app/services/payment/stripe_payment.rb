# frozen_string_literal: true

module Payment
  class StripePayment
    def self.checkout(args)
      Stripe::Checkout::Session.create({
                                         customer: args[:user].customer_id,
                                         payment_method_types: ['card'],
                                         line_items: [{ price: args[:price_id], quantity: 1 }],
                                         mode: 'subscription',
                                         success_url: args[:success_path],
                                         cancel_url: args[:cancel_path]
                                       })
    end

    def self.process_user_subscription(request)
      payload = request.body.read
      sig_header = request.env['HTTP_STRIPE_SIGNATURE']
      event = nil

      begin
        event = Stripe::Webhook.construct_event(
          payload, sig_header, ENV['STRIPE_SIGNING_SECRET']
        )
      rescue JSON::ParseError
        status 400
        return
      rescue Stripe::SignatureVerificationError => e
        Rails.logger.debug 'Signature error'
        Rails.logger.debug e
        return
      end

      case event.type
      when 'customer.created'
        customer = event.data.object
        user = User.find_by(email: customer.email)
        user.update(customer_id: customer.id)
      when 'customer.subscription.created', 'customer.subscription.updated'
        subscription = event.data.object
        user = User.find_by(customer_id: subscription.customer)
        user.update(
          subscription_status: subscription.status,
          subscription_start_date: Time.zone.at(subscription.current_period_start),
          subscription_end_date: Time.zone.at(subscription.current_period_end)
        )
      end
    end
  end
end
