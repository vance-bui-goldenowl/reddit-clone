# frozen_string_literal: true

module VotesHelper
  def kind_of_vote(user, votes)
    return 'None' unless user

    vote = votes.find_by(user_id: user.id)

    if vote
      vote.value == 'upvote' ? 'Upvote' : 'Downvote'
    else
      'None'
    end
  end
end
