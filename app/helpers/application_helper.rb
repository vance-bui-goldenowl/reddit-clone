# frozen_string_literal: true

module ApplicationHelper
  include Pagy::Frontend

  def svg_pack_tag(icon_name, options = {})
    file = File.read(Rails.root.join('app', 'javascript', 'images', "#{icon_name}.svg"))
    doc = Nokogiri::HTML::DocumentFragment.parse file
    svg = doc.at_css 'svg'

    options.each { |attr, value| svg[attr.to_s] = value }

    doc.to_html.html_safe
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
