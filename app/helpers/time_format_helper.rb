# frozen_string_literal: true

module TimeFormatHelper
  def days_from_now(future_time)
    num_of_days = (future_time.to_date - Time.zone.now.to_date).to_i

    pluralize(num_of_days, 'day')
  end
end
