# frozen_string_literal: true

module TagsHelper
  def find_tag_with_interest_and_community(interest, community)
    Tag.find_by(interest_id: interest.id, taggable_type: 'Community', taggable_id: community.id)
  end
end
