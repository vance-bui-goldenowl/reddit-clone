# frozen_string_literal: true

module CommentsHelper
  def format_comments_count(amount)
    amount.zero? ? '0 Comment' : "#{amount} #{'Comment'.pluralize(amount)}"
  end
end
