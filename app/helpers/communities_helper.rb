# frozen_string_literal: true

module CommunitiesHelper
  def user_status_in_community(user, community)
    community_user = CommunityUser.find_by(community_id: community.id, user_id: user.id)

    if community_user
      case community_user.status
      when 'approved'
        'Joined'
      when 'moderating'
        'Processing'
      when 'rejected'
        'Banned'
      end
    else
      'Join'
    end
  end
end
