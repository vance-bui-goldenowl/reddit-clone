# frozen_string_literal: true

module PostsHelper
  def post_has_images?(post)
    post.content.body.attachments.any? { |a| a.content_type.include?('image') }
  end

  def post_saved?(user = nil, post = nil)
    if user && post
      user.saved_posts.find_by(id: post.id).nil? ? false : true
    else
      false
    end
  end

  def votes_count(post)
    UserPoll.where(poll_option_id: post.poll_options).count
  end

  def option_voted(post)
    return unless user_signed_in? && post.post_type == 'poll'

    current_user.voted_option(post)
  end

  def post_title(post)
    post.title.truncate(60)
  end
end
