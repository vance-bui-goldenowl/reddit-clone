# frozen_string_literal: true

module Users
  class SettingsController < UsersBaseController
    def account; end

    def profile; end

    def subscriptions; end

    def update_info
      current_user.update(settings_params)
      flash[:notice] = "User's settings updated!"
      redirect_to profile_settings_path
    end

    private

    def settings_params
      params.require(:user).permit(:full_name, :description, :avatar)
    end
  end
end
