# frozen_string_literal: true

module Users
  class InterestsController < UsersBaseController
    before_action :prepare_interest

    def create
      current_user.add_interests(params[:interests_id])
      flash[:notice] = 'Your interests has been updated!'

      redirect_to root_path
    end

    def posts
      @pagy, @posts = pagy(Post.by_status('approved').where(community_id: @interest.communities).order(created_at: :desc))

      respond_to do |format|
        format.html
        format.json do
          render json: {
            nextPosts: render_to_string(
              partial: 'shared/posts_preview',
              locals: { posts: @posts, need_icon: true, display_pin: false },
              format: [:html]
            ),
            pagination: view_context.pagy_nav(@pagy)
          }
        end
      end
    end

    def communities
      @communities = @interest.communities
    end

    private

    def prepare_interest
      @interest = Interest.find(params[:interest_id])
    end
  end
end
