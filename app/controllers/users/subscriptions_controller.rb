# frozen_string_literal: true

module Users
  class SubscriptionsController < UsersBaseController
    before_action :authenticate_user!, only: %i[success]
    before_action :redirect_if_not_signed_in, only: %i[create]

    def new; end

    def create
      if current_user.active_subscription?
        redirect_to new_subscription_path
        flash[:alert] = 'Subscription status is currently active'
      else
        @checkout_session = Payment::StripePayment.checkout(
          price_id: params[:price_id],
          user: current_user,
          success_path: success_subscriptions_url,
          cancel_path: new_subscription_url
        )
      end
    end

    def success; end
  end
end
