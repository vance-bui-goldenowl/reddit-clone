# frozen_string_literal: true

module Users
  module Chatrooms
    class MessagesController < ChatroomsBaseController
      def create
        message = Message.create(message_params)
        message.chatroom.update(newest_message_sent_at: message.created_at)
      end

      private

      def message_params
        params.require(:message).permit(:content, :user_id, :chatroom_id)
      end
    end
  end
end
