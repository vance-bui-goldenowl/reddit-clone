# frozen_string_literal: true

module Users
  module Chatrooms
    class UsersController < ChatroomsBaseController
      def create
        chatroom = Chatroom.find(params[:id])
        chatroom.add_users(params[:user_id])
        flash[:notice] = 'New members added!'
        redirect_to chatroom_path(chatroom)
      end
    end
  end
end
