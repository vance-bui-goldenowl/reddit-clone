# frozen_string_literal: true

module Users
  module Posts
    class PostsBaseController < ApplicationController
      before_action :authenticate_user!
    end
  end
end
