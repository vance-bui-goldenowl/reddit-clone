# frozen_string_literal: true

module Users
  module Posts
    class VotesController < PostsBaseController
      before_action :authenticate_user!

      def create
        voteable_type = params[:vote][:voteable_type]
        voteable_id = params[:vote][:voteable_id].to_i
        value = params[:vote][:value].to_i

        @vote = current_user.votes.find_by(voteable_type: voteable_type, voteable_id: voteable_id)

        if @vote
          if @vote.value == value
            @vote.destroy
          else
            @vote.update(vote_params)
          end
        else
          @vote = current_user.votes.build(vote_params)
          @vote.save
        end
      end

      private

      def vote_params
        params.require(:vote).permit(:value, :voteable_type, :voteable_id)
      end
    end
  end
end
