# frozen_string_literal: true

module Users
  module Posts
    class PollVotesController < PostsBaseController
      before_action :prepare_post

      def create
        @user_poll = UserPoll.new(user_poll_params.merge(user_id: current_user.id))

        if @user_poll.save
          flash[:notice] = 'Poll voted!'
        else
          flash[:alert] = "Something's wrong, can't vote poll!"
        end

        redirect_back(fallback_location: post_path(@post))
      end

      private

      def prepare_post
        post_id = params[:id] || params[:post_id]
        @post = Post.find(post_id)
      end

      def user_poll_params
        params.require(:user_poll).permit(:poll_option_id)
      end
    end
  end
end
