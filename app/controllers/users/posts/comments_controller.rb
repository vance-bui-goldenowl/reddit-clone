# frozen_string_literal: true

module Users
  module Posts
    class CommentsController < PostsBaseController
      def create
        @comment = current_user.comments.build(comment_params)
        @post = @comment.post

        return unless @comment.save

        flash[:success] = 'Comment was successfully created!'

        unless current_user.id == @post.user.id
          ActionCable.server.broadcast("notification_#{@post.user.id}",
                                       {
                                         message: "u/#{current_user.full_name} replied to your post in r/#{@post.community.name}", post_id: @post.id
                                       })
        end

        redirect_to post_path(@post)
      end

      private

      def comment_params
        params.require(:comment).permit(:content, :parent_id).merge(post_id: params[:post_id])
      end
    end
  end
end
