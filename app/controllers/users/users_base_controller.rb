# frozen_string_literal: true

module Users
  class UsersBaseController < ApplicationController
    before_action :authenticate_user!
  end
end
