# frozen_string_literal: true

module Users
  module Communities
    class TagsController < CommunitiesBaseController
      skip_before_action :prepare_community

      def destroy
        @tag = Tag.find(params[:id])
        @tag.destroy

        flash[:notice] = "Community's topic removed"
        redirect_to community_path(@tag.taggable)
      end
    end
  end
end
