# frozen_string_literal: true

module Users
  module Communities
    class UsersController < CommunitiesBaseController
      def create
        if @community.add_user(current_user, 'member')
          flash[:notice] = 'Join request sent, please wait for review from moderator!'
        else
          flash[:alert] =
            'Your request is still being processed. If this take too longs, please contact moderators of this community'
        end

        redirect_to community_path(@community)
      end
    end
  end
end
