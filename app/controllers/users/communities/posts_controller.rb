# frozen_string_literal: true

module Users
  module Communities
    class PostsController < CommunitiesBaseController
      def new
        @post = Post.new(post_type: params[:type])
        2.times { @post.poll_options.build } if params[:type] == 'poll'
      end

      def create
        @post = current_user.posts.build(post_params.merge(post_type: params[:type], community_id: params[:community_id], status: 'moderating'))

        if @post.save
          flash[:success] = 'Post was successfully created, waiting for approve from moderators!'
          redirect_to post_path(@post)
        else
          flash[:alert] = "Something's wrong, post was not created."
          redirect_back(fallback_location: new_post_path(community_id: params[:community_id], type: params[:type]))
        end
      end

      private

      def post_params
        params.require(:post).permit(:title, :content, poll_options_attributes: [:name])
      end
    end
  end
end
