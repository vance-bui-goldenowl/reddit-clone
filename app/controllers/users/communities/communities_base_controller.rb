# frozen_string_literal: true

module Users
  module Communities
    class CommunitiesBaseController < ApplicationController
      before_action :prepare_community
      before_action :authenticate_user!

      def prepare_community
        community_id = params[:id] || params[:community_id]
        @community = Community.find(community_id)
      end
    end
  end
end
