# frozen_string_literal: true

module Users
  module Communtiies
    class CommunityUsersController < CommunitiesBaseController
      skip_before_action :prepare_community

      def toggle_status
        @community_user = CommunityUser.find(params[:id])

        if @community_user.status == 'moderating' || community_user_status_params[:status] != @community_user.status
          @community_user.update(community_user_status_params)
          flash[:notice] = "User #{@community_user.status}!"
        else
          @community_user.update(status: 'moderating')
          flash[:notice] = "User's status revert to moderating"
        end

        redirect_back(fallback_location: post_path(@community_user))
      end

      private

      def community_user_status_params
        params.require(:community_user).permit(:status)
      end
    end
  end
end
