# frozen_string_literal: true

module Users
  module Communities
    class InterestsController < CommunitiesBaseController
      def create
        @community.add_interests(params[:interests_id])
        flash[:notice] = "Community's topics updated!"

        redirect_to community_path(@community)
      end
    end
  end
end
