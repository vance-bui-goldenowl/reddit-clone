# frozen_string_literal: true

module Users
  class PostsController < UsersBaseController
    before_action :authenticate_user!, except: %i[show]
    before_action :prepare_post, only: %i[toggle_pinned toggle_status destroy]

    def show
      @post = Post.includes(:community, :user, comments: %i[user votes]).find(params[:id])

      mark_notifications_as_read
    end

    def new
      @post = Post.new
    end

    def create; end

    def destroy
      if @post
        @post.destroy
        redirect_to community_path(@post.community)
      else
        flash[:alert] = "Something wrong, can't delete post"
        redirect_to post_path(@post)
      end
    end

    def bookmark
      post = current_user.saved_posts.find_by(id: params[:id])
      message = ''
      is_saved = true

      if post.nil?
        post = Post.find(params[:id])
        current_user.saved_posts << post
        message = 'Post saved successfully.'
      else
        current_user.saved_posts.delete(post)
        message = 'Post unsaved successfully!'
        is_saved = false
      end

      respond_to do |format|
        format.html
        format.json do
          render json: {
            message: message,
            isSaved: is_saved
          }
        end
      end
    end

    def toggle_pinned
      @post.toggle(:pinned).save

      respond_to do |format|
        format.html
        format.json do
          render json: {
            message: @post.pinned ? 'Post pinned' : 'Pin removed from post',
            isPinned: @post.pinned
          }
        end
      end
    end

    def toggle_status
      if @post.status == 'moderating' || post_status_params[:status] != @post.status
        @post.update(post_status_params)
        flash[:notice] = "Post was #{@post.status}!"
      else
        @post.update(status: 'moderating')
        flash[:notice] = 'Post status changed to moderating'
      end

      redirect_back(fallback_location: post_path(@post))
    end

    private

    def prepare_post
      post_id = params[:id] || params[:post_id]
      @post = Post.find(post_id)
    end

    def mark_notifications_as_read
      @post.notifications_as_post_pg.where(recipient: current_user).mark_as_read!
    end

    def post_status_params
      params.require(:post).permit(:status)
    end
  end
end
