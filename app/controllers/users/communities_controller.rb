# frozen_string_literal: true

module Users
  class CommunitiesController < UsersBaseController
    before_action :authenticate_user!, only: %i[create]
    before_action :prepare_community, only: %i[show blocked edit update]

    def show
      redirect_to community_blocked_path(@community) and return unless (user_signed_in? && @community.can_be_show?(current_user)) || (!user_signed_in? && @community.community_type == 'public')

      @pinned_posts = @community.posts_by_status('approved').where(pinned: true)

      @pagy, @posts = pagy(@community.posts_by_status('approved').where(pinned: false).includes(%i[votes user rich_text_content]).order(created_at: :desc))

      @community_interests = @community.interests

      respond_to do |format|
        format.html
        format.json do
          render json: {
            nextPosts: render_to_string(
              partial: 'shared/posts_preview',
              locals: { posts: @posts, need_icon: false, display_pin: true },
              format: [:html]
            ),
            pagination: view_context.pagy_nav(@pagy)
          }
        end
      end
    end

    def create
      @community = Community.new(community_params)

      if @community.save
        @community.add_user(current_user, 'moderator')
        flash[:success] = 'Community was successfully created!'
        redirect_to community_path(@community)
      else
        flash[:alert] = "Can't create community, name was aready taken"
        redirect_back(fallback_location: root_path)
      end
    end

    def blocked; end

    def edit; end

    def update
      @community.update(community_update_params)
      flash[:notice] = 'Community settings updated!'
      redirect_to edit_community_path(@community)
    end

    private

    def prepare_community
      community_id = params[:id] || params[:community_id]
      @community = Community.find(community_id)
    end

    def community_params
      params.require(:community).permit(:name, :community_type)
    end

    def community_update_params
      params.require(:community).permit(:community_icon)
    end
  end
end
