# frozen_string_literal: true

module Users
  class ProfileController < UsersBaseController
    before_action :prepare_user_profile
    before_action :authenticate_user!, only: %i[saved upvoted downvoted]

    def overview
      @posts = @user.posts.includes(:votes, :rich_text_content, :community).where(status: 'approved')
    end

    def posts
      @posts = if user_signed_in? && @user == current_user
                 @user.posts.includes(:votes, :rich_text_content, :community)
               else
                 @user.posts.includes(:votes, :rich_text_content, :community).where(status: 'approved')
               end
    end

    def comments
      @comments = @user.comments.includes(post: [:community]).order(created_at: :desc)
    end

    def saved
      @saved_posts = @user.saved_posts.where(status: 'approved')
    end

    def upvoted
      set_posts_on_vote(:upvote)
    end

    def downvoted
      set_posts_on_vote(:downvote)
    end

    private

    def prepare_user_profile
      @user = User.find(params[:user_id])
    end

    def set_posts_on_vote(value)
      votes_id = @user.votes.where(value: value, voteable_type: 'Post').pluck(:voteable_id)
      @posts = Post.where(id: votes_id)
    end
  end
end
