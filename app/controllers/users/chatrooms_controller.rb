# frozen_string_literal: true

module Users
  class ChatroomsController < UsersBaseController
    skip_before_action :prepare_query
    skip_before_action :populate_communities
    skip_before_action :prepare_notifications
    skip_before_action :prepare_interests_all
    before_action :prepare_chatrooms

    def index; end

    def show
      @chatroom = Chatroom.includes(messages: [:user]).find(params[:id])

      if @chatroom.room_type == 'group'
        render template: 'chatrooms/show_group'
      else
        render template: 'chatrooms/show_direct'
      end
    end

    def show_direct
      @direct_user = User.find(params[:user_id])

      @direct_chatroom = Chatroom.includes(messages: [:user]).find_or_create_direct_chatroom(current_user, @direct_user)

      redirect_to chatroom_path(@direct_chatroom)
    end

    def create
      chatroom = Chatroom.new(chatroom_params.merge(users: [current_user], room_type: 'group'))

      if chatroom.save
        flash[:notice] = 'Chatroom was successfully created!'
        redirect_to chatroom_path(chatroom)
      else
        flash[:alert] = "Oops, something's wrong, can't create chatroom..."
        redirect_to chatrooms_path
      end
    end

    private

    def prepare_chatrooms
      @group_chatrooms = current_user.chatrooms.by_type('group').order(newest_message_sent_at: :desc)
      @direct_chatrooms = current_user.chatrooms.by_type('direct').order(newest_message_sent_at: :desc)
      @users = User.where.not(id: current_user)
    end

    def chatroom_params
      params.require(:chatroom).permit(:name)
    end
  end
end
