# frozen_string_literal: true

module Admins
  class CommunitiesController < AdminsBaseController
    def index
      @communities = Community.all
    end
  end
end
