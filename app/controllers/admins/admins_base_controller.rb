# frozen_string_literal: true

module Admins
  class AdminsBaseController < ApplicationController
    before_action :authenticate_user!
  end
end
