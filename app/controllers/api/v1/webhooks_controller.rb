# frozen_string_literal: true

module Api
  module V1
    class WebhooksController < ApplicationController
      skip_before_action :verify_authenticity_token

      def create
        Payment::StripePayment.process_user_subscription(request)

        render json: { message: 'success' }
      end
    end
  end
end
