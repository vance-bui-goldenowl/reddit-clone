# frozen_string_literal: true

class ModeratorsController < ApplicationController
  before_action :authenticate_user!

  def moderate_resources
    populate_status(params[:status])
  end

  private

  def populate_status(status)
    case params[:type]
    when 'posts'
      set_posts(status)
      render template: "moderators/posts_#{status}"
    when 'community_posts'
      set_community_posts(status)
      render template: "moderators/community_posts_#{status}"
    when 'requests'
      set_requests(status)
      render template: "moderators/requests_#{status}"
    else
      flash[:alert] = "Error! moderating data don't exists"
      redirect_to root_path
    end
  end

  def set_posts(post_status)
    @user = User.find(params[:id])
    moderating_communities = @user.moderating_communities.where("community_users.status": 'approved')
    @posts = Post.includes(:rich_text_content, :community).where(status: post_status,
                                                                 community_id: moderating_communities)
  end

  def set_community_posts(post_status)
    @community = Community.find(params[:id])
    @posts = @community.posts.where(status: post_status)
  end

  def set_requests(request_status)
    @community = Community.find(params[:id])

    @requests = if current_user.has_role? :admin
                  @community.community_users.where(status: request_status)
                else
                  # Select join requests from member of community, excluding moderators
                  CommunityUser.where(status: request_status, community_id: @community.id, user_id: User.joins(:roles).where('roles.name': 'member', 'roles.resource_type': 'Community', 'roles.resource_id': @community.id))
                end
  end
end
