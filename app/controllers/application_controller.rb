# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pagy::Backend

  before_action :prepare_query
  before_action :populate_communities, if: :user_signed_in?
  before_action :prepare_notifications, if: :user_signed_in?
  before_action :prepare_interests_all

  def redirect_if_not_signed_in
    return if user_signed_in?

    redirect_to new_user_session_path
    flash[:alert] = 'You need to login first!'
    nil
  end

  private

  def prepare_query
    @query = Interest.ransack(params[:q])
  end

  def populate_communities
    @new_community = Community.new
    communities = current_user.communities_all_roles
    @moderating_communities = communities[:moderating]
    @followed_communities = communities[:followed]
  end

  def prepare_notifications
    @notifications = Notification.where(recipient: current_user).newest_first.limit(5)
    @unread_count = @notifications.unread.count
  end

  def prepare_interests_all
    @interests_all = Interest.all
  end
end
