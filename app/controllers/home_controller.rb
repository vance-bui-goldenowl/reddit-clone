# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @posts = nil
    if user_signed_in?
      private_communities = current_user.communities.by_type('private')

      @posts = Post.joins(:community).where(status: 'approved', "communities.community_type": 'public').or(Post.joins(:community).where(status: 'approved', community: private_communities))
    else
      @posts = Post.joins(:community).where(status: 'approved', "communities.community_type": 'public')
    end

    @pagy, @posts = pagy(@posts.order(created_at: :desc))

    respond_to do |format|
      format.html
      format.json do
        render json: {
          nextPosts: render_to_string(
            partial: 'shared/posts_preview',
            locals: { posts: @posts, need_icon: true, display_pin: false },
            format: [:html]
          ),
          pagination: view_context.pagy_nav(@pagy)
        }
      end
    end
  end

  def search
    @interests = Interest.includes(:communities, tags: [:taggable]).ransack(params[:q]).result(distinct: true)
    @communities = Community.ransack(params[:q]).result(distinct: true)
  end
end
