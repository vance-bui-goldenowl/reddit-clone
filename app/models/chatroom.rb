# frozen_string_literal: true

class Chatroom < ApplicationRecord
  extend Enumerize

  validates :name, presence: true
  validates :room_type, presence: true
  enumerize :room_type, in: %i[group direct]

  has_many :messages, dependent: :destroy
  has_many :user_chatrooms, dependent: :destroy
  has_many :users, through: :user_chatrooms

  scope :by_type, ->(type) { where(room_type: type) }

  def users_outside_chatroom
    # Currently get all users, will change to current user's friends later
    User.where.not(id: users)
  end

  def self.find_or_create_direct_chatroom(user1, user2)
    Chatroom.find_by(name: "direct_chatroom_#{user1.id}_#{user2.id}") ||
      Chatroom.find_by(name: "direct_chatroom_#{user2.id}_#{user1.id}") ||
      Chatroom.create(
        name: "direct_chatroom_#{user1.id}_#{user2.id}", room_type: 'direct', users: [user1, user2]
      )
  end

  def add_users(user_ids)
    user_ids.each do |user_id|
      users << User.find(user_id)
    end
  end

  def first_user(except_id)
    users.where.not(id: except_id).first
  end
end
