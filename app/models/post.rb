# frozen_string_literal: true

class Post < ApplicationRecord
  validates :title, presence: true
  validates :status, presence: true

  extend Enumerize
  enumerize :status, in: %i[moderating approved rejected]
  enumerize :post_type, in: %i[post poll]

  belongs_to :user
  belongs_to :community
  has_rich_text :content
  has_many :votes, as: :voteable, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :poll_options, dependent: :destroy

  accepts_nested_attributes_for :poll_options

  scope :by_status, ->(status) { where(status: status) }
  scope :was_pinned, -> { where(pinned: true) }

  def first_image
    return content.body.attachments.first.url if content.body.attachments.first.attachable.content_type == 'image'

    content.body.attachments.first
  end

  # Create a custom method for pg
  def notifications_as_post_pg
    pg_sql = "SELECT \"notifications\".* FROM \"notifications\" WHERE (params::jsonb @> '{\"post\":{\"_aj_globalid\":\"gid://reddit-clone/Post/#{id}\"},\"_aj_symbol_keys\":[\"post\"]}'::jsonb)"
    notifications_array = Notification.find_by_sql(pg_sql)
    Notification.where(id: notifications_array.map(&:id))
  end
end
