# frozen_string_literal: true

class PollOption < ApplicationRecord
  belongs_to :post
  has_many :user_polls, dependent: :destroy
  has_many :polled_users, through: :user_polls, source: :user
end
