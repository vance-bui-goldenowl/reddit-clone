# frozen_string_literal: true

module SubscriptionConcern
  extend ActiveSupport::Concern

  included do
    def active_subscription?
      subscription_status == 'active' && (subscription_end_date.nil? ? false : subscription_end_date > Time.zone.now)
    end
  end
end
