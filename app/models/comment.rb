# frozen_string_literal: true

class Comment < ApplicationRecord
  validates :content, presence: true

  belongs_to :user
  belongs_to :post
  belongs_to :parent, class_name: 'Comment', optional: true
  has_many :comments, foreign_key: :parent_id
  has_many :votes, as: :voteable, dependent: :destroy

  after_create_commit :notify_recipient
  before_destroy :cleanup_notifications

  private

  def notify_recipient
    return if user_id == post.user.id

    CommentNotification.with(comment: self, post: post).deliver_later(post.user)
  end

  def cleanup_notifications
    notifications_as_comment_pg.destroy_all
  end

  # Create a custom method for pg
  def notifications_as_comment_pg
    pg_sql = "SELECT \"notifications\".* FROM \"notifications\" WHERE (params::jsonb @> '{\"comment\":{\"_aj_globalid\":\"gid://reddit-clone/Comment/#{id}\"},\"_aj_symbol_keys\":[\"comment\"]}'::jsonb)"
    notifications_array = Notification.find_by_sql(pg_sql)
    Notification.where(id: notifications_array.map(&:id))
  end
end
