# frozen_string_literal: true

class Community < ApplicationRecord
  extend Enumerize
  resourcify
  validates :name, presence: true, length: { maximum: 21 }, uniqueness: true
  validates :community_type, presence: true
  enumerize :community_type, in: %i[public private]

  has_many :community_users, dependent: :destroy
  has_many :users, through: :community_users
  has_many :posts, dependent: :destroy
  has_many :tags, as: :taggable, dependent: :destroy
  has_many :interests, through: :tags
  has_one_attached :community_icon

  scope :by_type, ->(type) { where(community_type: type) }

  def add_user(user, role)
    return if user_joined?(user)

    CommunityUser.create(community_id: id, user_id: user.id, status: role == 'moderator' ? 'approved' : 'moderating')
    user.add_role role.to_sym, self
  end

  def add_interests(interest_ids)
    interest_ids.each do |interest_id|
      Tag.find_or_create_by(taggable_type: 'Community', taggable_id: id, interest_id: interest_id)
    end
  end

  def user_joined?(user)
    users.find_by(id: user.id)
  end

  def posts_by_status(status)
    posts.where(status: status)
  end

  def can_be_show?(user)
    return false if community_users.find_by(user_id: user.id, status: 'rejected')

    community_type == 'public' || user.has_role?(:admin) || (community_type == 'private' && community_users.find_by(
      user_id: user.id, status: 'approved'
    ))
  end

  def moderators
    User.joins(:roles).where("roles.name": 'moderator', "roles.resource_type": 'Community', "roles.resource_id": id)
  end
end
