# frozen_string_literal: true

class Vote < ApplicationRecord
  extend Enumerize
  validates :value, presence: true
  enumerize :value, in: { upvote: 1, downvote: -1 }

  belongs_to :user
  belongs_to :voteable, polymorphic: true
end
