# frozen_string_literal: true

class Interest < ApplicationRecord
  has_many :tags, dependent: :destroy
  has_many :communities, through: :tags, source: :taggable, source_type: 'Community'
  has_many :users, through: :tags, source: :taggable, source_type: 'User'
end
