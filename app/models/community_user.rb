# frozen_string_literal: true

class CommunityUser < ApplicationRecord
  extend Enumerize
  enumerize :status, in: %i[moderating approved rejected]

  belongs_to :community
  belongs_to :user
end
