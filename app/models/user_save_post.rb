# frozen_string_literal: true

class UserSavePost < ApplicationRecord
  belongs_to :user
  belongs_to :post
end
