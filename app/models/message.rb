# frozen_string_literal: true

class Message < ApplicationRecord
  validates :content, presence: true

  belongs_to :user
  belongs_to :chatroom

  after_create :broadcast_message

  def broadcast_message
    ActionCable.server.broadcast("chatroom_channel_#{chatroom_id}", {
                                   message_content: content,
                                   chatroom_id: chatroom_id,
                                   user_id: user_id,
                                   username: user.full_name,
                                   avatar_url: user.avatar_url
                                 })
  end
end
