# frozen_string_literal: true

class UserPoll < ApplicationRecord
  belongs_to :user
  belongs_to :poll_option
end
