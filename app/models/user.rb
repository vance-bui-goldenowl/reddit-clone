# frozen_string_literal: true

class User < ApplicationRecord
  include SubscriptionConcern

  after_create :add_customer_id, unless: :skip_add_customer_id
  after_create :generate_random_username, unless: :full_name

  rolify
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :trackable,
         :omniauthable, omniauth_providers: %i[google_oauth2 facebook]

  has_many :community_users, dependent: :destroy
  has_many :communities, through: :community_users
  has_many :posts, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :user_save_posts, dependent: :destroy
  has_many :saved_posts, through: :user_save_posts, source: :post
  has_many :messages, dependent: :destroy
  has_many :user_chatrooms, dependent: :destroy
  has_many :chatrooms, through: :user_chatrooms
  has_many :notifications, as: :recipient, dependent: :destroy
  has_many :tags, as: :taggable, dependent: :destroy
  has_many :interests, through: :tags
  has_one_attached :avatar

  scope :all_except, ->(user) { where.not(id: user) }

  attr_accessor :skip_add_customer_id

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.full_name = auth.info.name
      user.avatar_url = auth.info.image
      user.skip_confirmation!
    end
  end

  def communities_all_roles
    {
      moderating: communities.where(id: roles.where(name: 'moderator').pluck(:resource_id)),
      followed: communities.where.not(id: roles.where(name: 'moderator').pluck(:resource_id))
    }
  end

  def moderating_communities
    communities.where(id: roles.where(name: 'moderator').pluck(:resource_id))
  end

  def generate_random_username
    update(full_name: Faker::FunnyName.two_word_name)
  end

  def add_customer_id
    update(customer_id: Stripe::Customer.create(email: email)['id'])
  end

  def admin_or_approved_moderator?(community)
    is_admin = has_role?(:admin)
    is_approved_moderator = roles.find_by(name: 'moderator', resource_type: 'Community',
                                          resource_id: community.id) && community_users.find_by(
                                            user_id: id, community_id: community.id, status: 'approved'
                                          )

    is_admin || is_approved_moderator
  end

  def voted_option(post)
    UserPoll.find_by(user_id: id, poll_option_id: post.poll_options)&.poll_option
  end

  def current_avatar
    avatar.attached? ? avatar : avatar_url
  end

  def add_interests(interest_ids)
    interest_ids.each do |interest_id|
      Tag.find_or_create_by(taggable_type: 'User', taggable_id: id, interest_id: interest_id)
    end
  end
end
