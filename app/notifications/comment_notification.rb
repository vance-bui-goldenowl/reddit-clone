# frozen_string_literal: true

class CommentNotification < Noticed::Base
  deliver_by :database
  # deliver_by :email, mailer: "CommentMailer"

  def message
    @post = Post.find(params[:comment][:post_id])
    @user = User.find(params[:comment][:user_id])

    "u/#{@user.full_name} replied to your post in r/#{@post.community.name}"
  end

  def url
    post_path(Post.find(params[:comment][:post_id]))
  end
end
