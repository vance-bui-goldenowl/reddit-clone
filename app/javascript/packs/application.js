require("@rails/ujs").start();
require("turbolinks").start();
require("@rails/activestorage").start();
require("channels");

import "stylesheets/application";
const images = require.context("../images", true);
const imagePath = (name) => images(name, true);

import "@fortawesome/fontawesome-free/css/all";

import "../controllers";
import { Application } from "@hotwired/stimulus";
const application = Application.start();
export { application };

require("trix");
require("@rails/actiontext");

// Turn this on when check subscription and push on production!
import "@stripe/stripe-js";

import * as Routes from "../routes";
window.Routes = Routes;
