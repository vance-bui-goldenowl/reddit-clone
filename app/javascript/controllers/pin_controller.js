import { Controller } from "@hotwired/stimulus";
import Rails from "@rails/ujs";

export default class extends Controller {
  static values = {
    url: String,
  };

  static targets = ["pinBtn"];

  processPin() {
    console.log("was clicked");
    Rails.ajax({
      type: "GET",
      url: this.urlValue,
      dataType: "json",
      success: (data) => {
        const messageContainer = document.createElement("div");
        messageContainer.classList.add(
          "flash__message",
          "fixed",
          "bottom-0",
          "left-0",
          "w-full",
          "bg-teal-100",
          "border",
          "border-teal-500",
          "text-teal-900",
          "px-4",
          "py-3",
          "rounded",
          "text-center",
          "z-50"
        );

        messageContainer.dataset.controller = "remove";
        messageContainer.dataset.action = "animationend->remove#remove";

        messageContainer.innerText = data.message;

        document.body.appendChild(messageContainer);

        if (data.isPinned) {
          this.pinBtnTarget.classList.add("text-green-500");
          this.pinBtnTarget.innerHTML =
            '<i class="fa-solid fa-thumbtack"></i> <strong>Pinned</strong>';
        } else {
          this.pinBtnTarget.classList.remove("text-green-500");
          this.pinBtnTarget.innerHTML =
            '<i class="fa-solid fa-thumbtack"></i> <strong>Pin Post</strong>';
        }
      },
    });
  }
}
