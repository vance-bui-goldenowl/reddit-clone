import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["modal"];

  connect() {
    this.modalTargets.forEach((target) => {
      target.classList.add("modal-hidden");
    });
  }

  showModal(e) {
    let eventId = e.target.id.split("-")[0];

    this.modalTargets.forEach((target) => {
      let modalId = target.id.split("-")[0];

      if (eventId === modalId) {
        target.classList.add("modal-active");
      }
    });
  }

  closeModal(e) {
    let eventId = e.target.id.split("-")[0];

    this.modalTargets.forEach((target) => {
      let modalId = target.id.split("-")[0];

      if (eventId === modalId) {
        target.classList.remove("modal-active");
        target.classList.add("modal-hidden");
      }
    });
  }
}
