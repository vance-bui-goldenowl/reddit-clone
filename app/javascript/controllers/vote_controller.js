import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["upvoteButton", "currentVotes", "downvoteButton"];

  upvoteOrRemove() {
    let upvoteActive =
      this.upvoteButtonTarget.classList.contains("text-redditOrange");
    let downvoteActive =
      this.downvoteButtonTarget.classList.contains("text-blue-500");

    if (upvoteActive) {
      this.upvoteButtonTarget.classList.remove("text-redditOrange");
      this.currentVotesTarget.classList.remove("text-redditOrange");
      this.currentVotesTarget.innerText =
        Number(this.currentVotesTarget.innerText) - 1;
    } else {
      this.upvoteButtonTarget.classList.add("text-redditOrange");
      this.currentVotesTarget.classList.add("text-redditOrange");
      if (downvoteActive) {
        this.downvoteButtonTarget.classList.remove("text-blue-500");
        this.currentVotesTarget.classList.remove("text-blue-500");
        this.currentVotesTarget.innerText =
          Number(this.currentVotesTarget.innerText) + 2;
      } else {
        this.currentVotesTarget.innerText =
          Number(this.currentVotesTarget.innerText) + 1;
      }
    }
  }

  downvoteOrRemove() {
    let upvoteActive =
      this.upvoteButtonTarget.classList.contains("text-redditOrange");
    let downvoteActive =
      this.downvoteButtonTarget.classList.contains("text-blue-500");

    if (downvoteActive) {
      this.downvoteButtonTarget.classList.remove("text-blue-500");
      this.currentVotesTarget.classList.remove("text-blue-500");
      this.currentVotesTarget.innerText =
        Number(this.currentVotesTarget.innerText) + 1;
    } else {
      this.downvoteButtonTarget.classList.add("text-blue-500");
      this.currentVotesTarget.classList.add("text-blue-500");
      if (upvoteActive) {
        this.upvoteButtonTarget.classList.remove("text-redditOrange");
        this.currentVotesTarget.classList.remove("text-redditOrange");
        this.currentVotesTarget.innerText =
          Number(this.currentVotesTarget.innerText) - 2;
      } else {
        this.currentVotesTarget.innerText =
          Number(this.currentVotesTarget.innerText) - 1;
      }
    }
  }
}
