import { Controller } from "@hotwired/stimulus";
import Rails from "@rails/ujs";

export default class extends Controller {
  static values = {
    url: String,
  };

  static targets = ["saveBtn"];

  processSave() {
    Rails.ajax({
      type: "GET",
      url: this.urlValue,
      dataType: "json",
      success: (data) => {
        const messageContainer = document.createElement("div");
        messageContainer.classList.add(
          "flash__message",
          "fixed",
          "bottom-0",
          "left-0",
          "w-full",
          "bg-teal-100",
          "border",
          "border-teal-500",
          "text-teal-900",
          "px-4",
          "py-3",
          "rounded",
          "text-center",
          "z-50"
        );

        messageContainer.dataset.controller = "remove";
        messageContainer.dataset.action = "animationend->remove#remove";

        messageContainer.innerText = data.message;

        document.body.appendChild(messageContainer);

        if (!data.isSaved) {
          const icon = document.createElement("i");
          icon.classList.add("fa-regular", "fa-bookmark", "text-2xl");
          const strongText = document.createElement("strong");
          strongText.innerText = "Save";
          this.saveBtnTarget.innerHTML = "";
          this.saveBtnTarget.appendChild(icon);
          this.saveBtnTarget.appendChild(strongText);
        } else {
          const icon = document.createElement("i");
          icon.classList.add("fa-solid", "fa-bookmark", "text-2xl");
          const strongText = document.createElement("strong");
          strongText.innerText = "Unsave";
          this.saveBtnTarget.innerHTML = "";
          this.saveBtnTarget.appendChild(icon);
          this.saveBtnTarget.appendChild(strongText);
        }
      },
      error: (error, status) => {
        if (status == "Unauthorized") {
          document.getElementById("login-open").click();

          const errorMessageContainer = document.createElement("div");
          errorMessageContainer.classList.add(
            "flash__message",
            "fixed",
            "bottom-0",
            "left-0",
            "w-full",
            "bg-red-100",
            "border",
            "border-red-400",
            "text-red-700",
            "px-4",
            "py-3",
            "rounded",
            "text-center",
            "z-50"
          );

          errorMessageContainer.dataset.controller = "remove";
          errorMessageContainer.dataset.action = "animationend->remove#remove";

          errorMessageContainer.innerText =
            "You need to login to perform this action!";

          document.body.appendChild(errorMessageContainer);
        }
      },
    });
  }
}
