import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["optionContainer"];

  addOption() {
    let lastOptionId = Number(this.optionContainerTarget.dataset.lastOptionId);
    console.log(lastOptionId);
    const lastOptionContainer = document.getElementById(
      `optionContainer${lastOptionId}`
    );

    const optionContainer = document.createElement("div");
    optionContainer.classList.add("flex", "items-center", "gap-2");
    optionContainer.setAttribute("id", `optionContainer${lastOptionId + 1}`);

    const optionIcon = document.createElement("i");
    optionIcon.classList.add("fa-solid", "fa-bars", "text-xl", "text-gray-500");

    const optionInput = document.createElement("input");
    optionInput.classList.add(
      "w-full",
      "py-2",
      "px-4",
      "focus:outline-none",
      "focus:ring-1",
      "focus:ring-black",
      "rounded-md",
      "dark:bg-gray-700",
      "dark:text-white",
      "dark:ring-white"
    );
    optionInput.setAttribute(
      "id",
      `post_poll_options_attributes_${lastOptionId + 1}_name`
    );
    optionInput.setAttribute("type", "text");
    optionInput.setAttribute(
      "name",
      `post[poll_options_attributes][${lastOptionId + 1}][name]`
    );
    optionInput.setAttribute("required", "required");
    optionInput.setAttribute("placeholder", `Option ${lastOptionId + 2}`);

    optionContainer.appendChild(optionIcon);
    optionContainer.appendChild(optionInput);

    lastOptionContainer.after(optionContainer);

    this.optionContainerTarget.setAttribute(
      "data-last-option-id",
      lastOptionId + 1
    );
  }
}
