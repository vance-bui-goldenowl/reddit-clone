import { Controller } from "@hotwired/stimulus";

// TODO: FIX BUG LINK RETURN WRONG WHEN TOO LONG

export default class extends Controller {
  static targets = ["clickBtn"];
  static values = {
    url: String,
  };

  saveContent() {
    navigator.clipboard.writeText(this.urlValue);

    this.clickBtnTarget.innerText = "COPIED";
  }
}
