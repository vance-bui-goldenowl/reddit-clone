import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["toggleCircle"];

  toggleDarkmode() {
    if (document.documentElement.classList.contains("dark")) {
      document.documentElement.classList.remove("dark");
      localStorage.setItem("theme", "light");
    } else {
      document.documentElement.classList.add("dark");
      localStorage.setItem("theme", "dark");
    }

    this.toggleButton();
  }

  toggleButton() {
    this.toggleCircleTarget.classList.toggle("ml-auto");
  }

  connect() {
    const userTheme = localStorage.getItem("theme");
    const systemTheme = window.matchMedia(
      "(prefers-color-scheme: dark)"
    ).matches;

    if (userTheme == "dark" || (!userTheme && systemTheme)) {
      document.documentElement.classList.add("dark");
      this.toggleCircleTarget.classList.add("ml-auto");
    } else {
      this.toggleCircleTarget.classList.remove("ml-auto");
    }
  }
}
