import { Controller } from "@hotwired/stimulus";
import Rails from "@rails/ujs";

// TODO: FIX BUG LINK RETURN WRONG WHEN TOO LONG

export default class extends Controller {
  static targets = ["container", "pagination"];

  initialize() {
    let scrollCalled = false;
  }

  scroll() {
    let loadPoint = 150;

    if (
      window.innerHeight + window.pageYOffset <
      document.body.offsetHeight - loadPoint
    ) {
      this.scrollCalled = false;
    }

    if (this.scrollCalled) {
      return;
    }

    let nextPageLink = this.paginationTarget.querySelector("a[rel='next']");

    if (nextPageLink == null) {
      return;
    }

    let nextPageUrl = nextPageLink.href;
    if (
      window.innerHeight + window.pageYOffset >=
      document.body.offsetHeight - loadPoint
    ) {
      this.loadMore(nextPageUrl);
      this.scrollCalled = true;
    }
  }

  loadMore(url) {
    Rails.ajax({
      type: "GET",
      url: url,
      dataType: "json",
      success: (data) => {
        this.containerTarget.insertAdjacentHTML("beforeend", data.nextPosts);
        this.paginationTarget.innerHTML = data.pagination;
      },
    });
  }
}
