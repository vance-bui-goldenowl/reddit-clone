import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["displayable"];

  toggle() {
    this.displayableTarget.classList.toggle("displayable");
  }
}
