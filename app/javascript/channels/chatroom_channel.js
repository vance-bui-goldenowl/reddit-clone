import consumer from "./consumer";

document.addEventListener("turbolinks:load", () => {
  const messagesContainer = document.getElementById("messages-container");

  if (messagesContainer != null) {
    const chatroomId = messagesContainer.getAttribute("data-chatroom-id");

    consumer.subscriptions.create(
      { channel: "ChatroomChannel", chatroom_id: chatroomId },
      {
        connected() {},

        disconnected() {
          // Called when the subscription has been terminated by the server
        },

        received(data) {
          const currentUserId = messagesContainer.getAttribute(
            "data-chatroom-user-id"
          );

          if (currentUserId == data.user_id) {
            const messageContainer = document.createElement("div");
            messageContainer.classList.add(
              "ml-auto",
              "bg-blue-500",
              "text-white",
              "rounded-full",
              "py-2",
              "px-4"
            );

            const message = document.createElement("p");
            message.classList.add("text-lg");
            message.innerText = data.message_content;

            messageContainer.appendChild(message);
            messagesContainer.append(messageContainer);

            const messageForm = document.getElementById("message-form");
            messageForm.reset();
          } else {
            const messageContainer = document.createElement("div");
            messageContainer.classList.add("flex", "flex-col");

            const username = document.createElement("div");
            username.classList.add("pl-14", "text-gray-500");
            username.innerText = data.username;

            const avatarAndContent = document.createElement("div");
            avatarAndContent.classList.add("flex", "items-center", "gap-3");

            const avatarContainer = document.createElement("div");
            avatarContainer.classList.add(
              "w-10",
              "h-10",
              "bg-gray-500",
              "rounded-full",
              "overflow-hidden"
            );

            const avatar = document.createElement("img");
            avatar.classList.add("object-cover");
            if (data.avatar_url) {
              avatar.src = data.avatar_url;
            }

            const content = document.createElement("p");
            content.classList.add("text-lg");
            content.innerText = data.message_content;

            avatarContainer.appendChild(avatar);

            avatarAndContent.appendChild(avatarContainer);
            avatarAndContent.appendChild(content);

            messageContainer.appendChild(username);
            messageContainer.appendChild(avatarAndContent);

            messagesContainer.append(messageContainer);
          }

          messagesContainer.scrollTop = messagesContainer.scrollHeight;

          const chatroomLink = document.getElementById(
            `chatroom-${data.chatroom_id}`
          );

          const chatroomsContainer = chatroomLink.parentNode;
          chatroomsContainer.insertBefore(
            chatroomLink,
            chatroomsContainer.children[0]
          );

          const chatroomLastMessage = document.getElementById(
            `chatroom-${data.chatroom_id}__last-message`
          );
          chatroomLastMessage.innerText = data.message_content;
        },
      }
    );
  }
});
