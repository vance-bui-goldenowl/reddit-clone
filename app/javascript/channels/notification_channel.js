import consumer from "./consumer";
import { post_path } from "../routes";

document.addEventListener("turbolinks:load", () => {
  const notificationIcon = document.getElementById("notification-icon");
  if (Number(notificationIcon.innerText) == 0) {
    notificationIcon.classList.remove("flex");
    notificationIcon.classList.add("hidden");
  }

  if (notificationIcon != null) {
    const user_id = notificationIcon.getAttribute("data-user-id");

    consumer.subscriptions.create(
      { channel: "NotificationChannel", user_id: user_id },
      {
        connected() {},

        disconnected() {},

        received(data) {
          if (Number(notificationIcon.innerText) == 0) {
            notificationIcon.classList.remove("hidden");
            notificationIcon.classList.add("flex");
          }

          notificationIcon.innerText = Number(notificationIcon.innerText) + 1;

          const avatarOuterContainer = document.createElement("div");
          const avatarInnerContainer = document.createElement("div");
          avatarInnerContainer.classList.add(
            "w-12",
            "h-12",
            "rounded-full",
            "bg-gray-500"
          );
          avatarOuterContainer.appendChild(avatarInnerContainer);

          const notificationContent = document.createElement("span");
          notificationContent.innerText = data.message;

          const notificationContainer = document.createElement("a");
          let message_url = post_path({ id: data.post_id });
          notificationContainer.href = message_url;
          notificationContainer.classList.add("flex", "items-center", "gap-2");
          notificationContainer.appendChild(avatarOuterContainer);
          notificationContainer.appendChild(notificationContent);

          const notificationsContainer = document.getElementById(
            "notifications-container"
          );
          notificationsContainer.prepend(notificationContainer);
        },
      }
    );
  }
});
