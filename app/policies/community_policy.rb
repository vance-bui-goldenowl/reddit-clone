# frozen_string_literal: true

class CommunityPolicy < ApplicationPolicy
  def show?
    record.community_type == 'public' ||
      (!user.nil? && record.community_type == 'private' && record.users.any? { |u| u.id == user.id })
  end
end
